# Generic Tmux UI with Tempus Summer (Tempus themes) colours
#
# Copyright (C) 2020  Protesilaos Stavrou <info@protesilaos.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
## Commentary:
#
# Rudimentary configurations for Tmux that implement a theme as
# demonstrated in the Tempus themes' main git repository:
# https://gitlab.com/protesilaos/tempus-themes.
#
# To make these work as intended, you need a terminal emulator with the
# corresponding Tempus theme.
#
# NOTE on colour naming conventions.  Any other colour I define herein
# follows the numbering of the 16 first escape sequences.  In short
# these are as follows (counting from 0-15 with "br[colour name]"
# standing for the "bright" variant):
#
# black, red, green, yellow, blue, magenta, cyan, white
# brblack, brred, bryellow, brblue, brmagenta, brcyan, brwhite
#
## Code:

# Default window colours are inherited from the underlying terminal.
# This also allows for a transparent background.
set -g window-style "bg=default,fg=default"
set -g window-active-style "bg=default,fg=default"

# Pane styles.  Setting the bg to "default" allows for transparency, if
# the underlying terminal emulator is configured accordingly.
set -g pane-border-style "bg=#2a2d43,fg=#352f49"
set -g pane-active-border-style "bg=#352f49,fg=#919ab9"

# Copy mode styles.  This governs such things as the visual selection
# block and the search results in the scroll buffer.  I choose to avoid
# setting to plain "reverse" because it is visually identical to Vim's
# selection, which can be confusing sometimes.
set -g mode-style "bg=#352f49,fg=#3dab95,bold,underscore"

# Status line styles.  Used when displaying status messages and when
# using the command prompt.
set -g message-style "bg=default,fg=default,bold"

# Clock mode options (I seldom use this, but okay).
set -g clock-mode-style 24 # time format
set -g clock-mode-colour "#919ab9"

# Status bar
# ----------

# Interval to update status bar in seconds.  Only affects some elements.
# Test on a case-by-case basis.  My setup is not affected.
set -g status-interval 120

# Status bar general options about placement and overall style.
set -g status on
set -g status-style "fg=#a0abae,bg=#2a2d43"
set -g status-position top
set -g status-justify left # this concerns the window list

# NOTE the status bar has three components: left, right, centre.  The
# latter is hard-coded to the window list component.  So instead of
# status-centre, we have to modify the 'window-status' (see below).
set -g status-left-length "100"
set -g status-right-length "100"

# Window status.  Always placed in the centre if 'status-justify
# "centre"'.  Otherwise it floats next to either of the two remaining
# components of the status bar.
#
# NOTE trigger alert to test bell: echo -e '\a'
set -g window-status-separator ""
set -g window-status-format " #I: #W #{?window_flags,#F , }"
set -g window-status-current-format "#[bold][#I: #W #F]"
set -g window-status-activity-style ""
set -g window-status-bell-style "fg=#f76f6e,bg=default"
set -g window-status-style "fg=#919ab9,bg=#352f49"
set -g window-status-current-style "fg=#a0abae,bg=#202c3d"
set -g window-status-last-style ""

# Status left and right
set -g status-left "#[bold]#{?pane_synchronized,#[fg=#cc84ad] sync on ,}#{?pane_in_mode,#[fg=#609fda] #{pane_mode}#{?selection_present, selecting,}#{?rectangle_toggle, rectangle,} ,}"
set -g status-right "#[bg=default,fg=default]#{?client_prefix,#[fg=#4eac6d]#[bg=default]#[reverse],} #S #{session_id} #{?client_prefix,,}"
